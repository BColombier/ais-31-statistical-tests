import numpy as np
from multiprocessing import Pool, cpu_count
import logging as log


def T0(raw_input_data):
    """
    AIS-31 T0 test
    Check that 48-bit blocks are unique
    """
    blocks_48_bit = np.reshape(raw_input_data, (48, -1))
    return np.shape(np.unique(blocks_48_bit, axis=1)) == np.shape(blocks_48_bit)


def T1(block):
    """
    AIS-31 T1 test
    Check that the number of ones is within a range
    """
    def get_value(block):
        return np.count_nonzero(block == 1)

    def test_value(value):
        return 9654 < value < 10346

    value = get_value(block)
    return value, test_value(value)


def T2(block):
    """
    AIS-31 T2 test
    Check the number of 4-bit blocks
    """
    def get_value(block):
        # Reshape to blocks of 4 bits
        four_bit_blocks = np.pad(np.reshape(block, (-1, 4)), (4, 0), 'constant', constant_values=0)[4:]
        # Count the number of occurences each block of four bits
        _, counts = np.unique(np.packbits(four_bit_blocks, axis=1), return_counts=True)
        return 16 / 5000 * np.sum(np.square(counts))-5000

    def test_value(value):
        return 1.03 < value < 57.4

    value = get_value(block)
    return value, test_value(value)


def T3(block):
    """
    AIS-31 T3 test
    Check runs length
    """
    def occurences(string, sub):
        """
        Helper function to compute the number of occurences
        of a substring in a string with overlappings
        """
        count = 0
        start = 0
        while True:
            start = string.find(sub, start)+1
            if start > 0:
                count += 1
            else:
                return count

    def get_value(block):
        big_string = block.tobytes()
        nbs_0 = []
        nbs_1 = []
        for i in range(1, 34):
            if i < 7:
                nbs_0.append(occurences(big_string, b'\x01'+i * b'\x00'+b'\x01'))
                nbs_1.append(occurences(big_string, b'\x00'+i * b'\x01'+b'\x00'))
            else:
                nbs_0[-1] += occurences(big_string, b'\x01'+i * b'\x00'+b'\x01')
                nbs_1[-1] += occurences(big_string, b'\x00'+i * b'\x01'+b'\x00')
            if big_string.startswith(i*b'\x00'+b'\x01'):
                nbs_0[-1] += 1
            elif big_string.startswith(i*b'\x01'+b'\x00'):
                nbs_1[-1] += 1
            if big_string.endswith(b'\x01'+i*b'\x00'):
                nbs_0[-1] += 1
            elif big_string.endswith(b'\x00'+i*b'\x01'):
                nbs_1[-1] += 1
        return nbs_0, nbs_1

    def test_value(nbs_0, nbs_1):
        for_zeros = np.logical_and.reduce(np.logical_and(np.greater(nbs_0, [2267, 1079, 502, 223, 90, 90]),
                                                         np.less(nbs_0, [2733, 1421, 748, 402, 223, 223])))
        for_ones = np.logical_and.reduce(np.logical_and(np.greater(nbs_1, [2267, 1079, 502, 223, 90, 90]),
                                                        np.less(nbs_1, [2733, 1421, 748, 402, 223, 223])))
        return for_zeros is np.bool_(True) and for_ones is np.bool_(True)

    nbs_0, nbs_1 = get_value(block)
    return nbs_0, nbs_1, test_value(nbs_0, nbs_1)


def T4(block):
    """
    AIS-31 T4 test
    Count sequences of 34 identical bytes
    """
    def get_value(block):
        # Convert to long string of bytes
        big_string = block.tobytes()
        # Count sequences of 34 identical bytes
        cnt_34_0 = big_string.count(34*b'\x00')
        cnt_34_1 = big_string.count(34*b'\x01')
        return cnt_34_0, cnt_34_1

    def test_value(value_0, value_1):
        return value_0 == 0 and value_1 == 0

    cnt_34_0, cnt_34_1 = get_value(block)
    return cnt_34_0, cnt_34_1, test_value(cnt_34_0, cnt_34_1)


def T5(block):
    """
    AIS-31 T5 test
    Autocorrelation test
    """
    def get_value(block):
        Z = np.array([np.sum(np.bitwise_xor(block[:5000], block[tau:5000+tau])) for tau in range(1, 5001)])
        # Surprisingly, this is slower Z = np.sum(np.bitwise_xor(block[:5000], block[shifts_5000]), axis=1)
        tau_0 = np.argmax(np.abs(np.subtract(Z, np.full(5000, 2500))))+1
        Z_2 = np.sum(np.bitwise_xor(block[10000:15000], block[10000+tau_0:15000+tau_0]))
        return Z_2

    def test_value(value):
        return 2326 < value < 2674

    value = get_value(block)
    return value, test_value(value)


def procedure_A(raw_input_data, cpu):
    """
    AIS-31 Procedure A
    Runs tests T0 to T5
    """
    log.info("Starting Procedure A")
    result_T0 = T0(raw_input_data[:48*2**16])
    # blocks_T1_T5 = np.reshape(raw_input_data[48*2**16:48*2**16+20000*257], (257, 20000))
    blocks_T1_T5 = np.reshape(raw_input_data[:20000*257], (257, 20000))
    log.info("Performing T1-T5 tests")
    if cpu == 1:
        results_T1 = []
        results_T2 = []
        results_T3 = []
        results_T4 = []
        results_T5 = []
        for index, block in enumerate(blocks_T1_T5):
            if index > 257:
                break
            else:
                results_T1.append(T1(block))
                results_T2.append(T2(block))
                results_T3.append(T3(block))
                results_T4.append(T4(block))
                results_T5.append(T5(block))
    else:
        if cpu > cpu_count():
            log.warning("You do not have that many available CPUs")
        elif cpu < cpu_count():
            log.warning("You do not exploit all available CPUs")
        pool = Pool(cpu)
        results_T1 = pool.map(T1, blocks_T1_T5)
        results_T2 = pool.map(T2, blocks_T1_T5)
        results_T3 = pool.map(T3, blocks_T1_T5)
        results_T4 = pool.map(T4, blocks_T1_T5)
        results_T5 = pool.map(T5, blocks_T1_T5)
    log.info("Procedure A finished")
    return result_T0, results_T1, results_T2, results_T3, results_T4, results_T5
