import numpy as np
import os
import logging as log
import platform


def write_results_to_file(input_files, results_procedure_A, results_procedure_B, output_filename, output_format, details_level, PLL):
    """
    Write AIS-31 statistical tests results to a file (CSV and/or PDF)
    """
    def min_distance(fref, m0, n0, m1, n1):
        import sympy
        Km = m1*n0
        Kd = m0*n1
        period_jitter_ratio = np.sqrt(2)*2/1000

        confidence_interval = 3
        var_duty_cycle = 0.1

        threshold = max(1, np.floor(2*confidence_interval*period_jitter_ratio*Kd))
        if threshold == 1:
            return 1
        else:
            jmin = np.ceil((-(2*var_duty_cycle+confidence_interval*period_jitter_ratio)*Kd+1)/2)
            jmax = np.floor(((2*var_duty_cycle+confidence_interval*period_jitter_ratio)*Kd+1)/2)

            dist = np.mod(sympy.mod_inverse(2*Km, Kd)*(2*np.arange(jmin, jmax+1)-1), Kd)
            dist = [d-Kd if d > Kd/2 else d for d in dist]

            dist_1 = np.mod(sympy.mod_inverse(Km, Kd)*(np.arange(1, threshold)), Kd)
            dist_1 = [d-Kd if d > Kd/2 else d for d in dist_1]
            return int(min(min(np.abs(dist)), min(np.abs(dist_1))))

    def format_results(input_file, results_procedure_A, results_procedure_B, details_level, PLL):
        verdict = {True: "PASS",
                   False: "FAIL",
                   "ERROR": "ERROR"}
        formatted_results = []
        result_T0, results_T1, results_T2, results_T3, results_T4, results_T5 = results_procedure_A
        result_T6, result_S2, result_S3, result_S4, result_T8 = results_procedure_B
        if PLL:
            m0, n0, m1, n1 = map(int, input_file.split("-")[-1].replace(".bin", "").split("_"))
            pvco = 2
            fref = 24
            c0 = c1 = 1
            f_OUT_0 = round(fref*m0/(n0*c0), 2)
            f_OUT_1 = round(fref*m1/(n1*c1), 2)
            f_PFD_0 = round(fref/n0, 2)
            f_PFD_1 = round(fref/n1, 2)
            f_VCO_0 = round(fref*m0*pvco/n0, 2)
            f_VCO_1 = round(fref*m1*pvco/n1, 2)
            Km = m1*n0
            Kd = m0*n1
            S = round(fref*m0*m1/1000000, 4)
            R = round(fref/(n0*n1), 4)
            d_min = min_distance(fref, m0, n0, m1, n1)
        if details_level == 1:
            T1, T2, T3, T4, T5 = True, True, True, True, True
            for result_T1, result_T2, result_T3, result_T4, result_T5 in zip(results_T1, results_T2, results_T3, results_T4, results_T5):
                T1 &= result_T1[-1]
                T2 &= result_T2[-1]
                T3 &= result_T3[-1]
                T4 &= result_T4[-1]
                T5 &= result_T5[-1]
            formatted_results.append([os.path.join(*os.path.split(input_file)[-2:]),
                                      verdict[result_T0],
                                      verdict[T1],
                                      verdict[T2],
                                      verdict[T3],
                                      verdict[T4],
                                      verdict[T5],
                                      verdict[result_T6[-1]],
                                      verdict[result_S2[-1]],
                                      verdict[result_S3[-2] & result_S3[-1]],
                                      verdict[result_S4[-4] & result_S4[-3] & result_S4[-2] & result_S4[-1]],
                                      verdict[result_T8[-1]]])
        elif details_level == 2:
            T1, T2, T3, T4, T5 = 0, 0, 0, 0, 0
            for result_T1, result_T2, result_T3, result_T4, result_T5 in zip(results_T1, results_T2, results_T3, results_T4, results_T5):
                T1 += result_T1[-1]
                T2 += result_T2[-1]
                T3 += result_T3[-1]
                T4 += result_T4[-1]
                T5 += result_T5[-1]
            if PLL:
                formatted_results.append([os.path.join(*os.path.split(input_file)[-2:]),
                                          m0,
                                          n0,
                                          f_OUT_0,
                                          m1,
                                          n1,
                                          f_OUT_1,
                                          Km,
                                          Kd,
                                          f_PFD_0,
                                          f_PFD_1,
                                          f_VCO_0,
                                          f_VCO_1,
                                          R,
                                          S,
                                          d_min,
                                          verdict[result_T0],
                                          "{}/257".format(T1),
                                          "{}/257".format(T2),
                                          "{}/257".format(T3),
                                          "{}/257".format(T4),
                                          "{}/257".format(T5),
                                          verdict[result_T6[-1]],
                                          verdict[result_S2[-1]],
                                          verdict[result_S3[-2] & result_S3[-1]],
                                          verdict[result_S4[-4] & result_S4[-3] & result_S4[-2] & result_S4[-1]]])
            else:
                formatted_results.append([os.path.join(*os.path.split(input_file)[-2:]),
                                          verdict[result_T0],
                                          "{}/257".format(T1),
                                          "{}/257".format(T2),
                                          "{}/257".format(T3),
                                          "{}/257".format(T4),
                                          "{}/257".format(T5),
                                          verdict[result_T6[-1]],
                                          verdict[result_S2[-1]],
                                          verdict[result_S3[-2] & result_S3[-1]],
                                          verdict[result_S4[-4] & result_S4[-3] & result_S4[-2] & result_S4[-1]]])
            if result_T8[-1] == "ERROR":
                formatted_results[-1].append("ERROR")
            else:
                formatted_results[-1].append(round(result_T8[0], 5))
        return formatted_results

    def write_results(output_filename, output_format, list_formatted_results, PLL):
        if output_format == "csv" or output_format == "both":
            with open(output_filename+".csv", 'a+') as results_file:
                results_file.write(",".join(['Input file', 'A-T0', 'A-T1', 'A-T2', 'A-T3', 'A-T4', 'A-T5', 'B-Step1(T6)', 'B-Step2', 'B-Step3', 'B-Step4', 'B-T8'])+"\n")
                for formatted_result in list_formatted_results:
                    print(formatted_result)
                    results_file.write(",".join(map(str, formatted_result[0]))+"\n")
        if output_format == "pdf" or output_format == "both":
            if platform.system() == 'Windows':
                log.error("Cannot generate PDFs under Windows for the moment, fallback to CSVx")
                write_results(output_filename, "csv", list_formatted_results, PLL)
                return -1
            else:
                from reportlab.lib import colors
                from reportlab.lib.pagesizes import A4, landscape
                from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
            doc = SimpleDocTemplate("{}.pdf".format(output_filename),
                                    pagesize=landscape(A4),
                                    topMargin=20,
                                    bottomMargin=20,
                                    leftMargin=20,
                                    rightMargin=20)
            elements = []
            final_table = []
            if PLL:
                header = ['Input file',
                          'm0',
                          'n0',
                          'f_OUT_0',
                          'm1',
                          'n1',
                          'f_OUT_1',
                          'Km',
                          'Kd',
                          'f_PFD_0',
                          'f_PFD_1',
                          'f_VCO_0',
                          'f_VCO_1',
                          'R',
                          'S',
                          'd_min',
                          'A-T0', 'A-T1', 'A-T2', 'A-T3', 'A-T4',
                          'A-T5', 'B-Step1(T6)', 'B-Step2', 'B-Step3', 'B-Step4', 'B-T8']
            else:
                header = ['Input file', 'A-T0', 'A-T1', 'A-T2', 'A-T3', 'A-T4',
                          'A-T5', 'B-Step1(T6)', 'B-Step2', 'B-Step3', 'B-Step4', 'B-T8']
            elements.append(header)
            for formatted_results in list_formatted_results:
                elements.extend(formatted_results)
            t = Table(elements, repeatRows=1, rowHeights=13)
            t.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                   ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                   ('LEFTPADDING', (0, 0), (-1, -1), 2),
                                   ('RIGHTPADDING', (0, 0), (-1, -1), 2),
                                   ('TOPPADDING', (0, 0), (-1, -1), 2),
                                   ('BOTTOMPADDING', (0, 0), (-1, -1), 2),
                                   ('FONTSIZE', (0, 0), (-1, -1), 6)]))
            for row_index, row in enumerate(elements):
                for col_index, entry in enumerate(row):
                    if entry == "PASS" or entry == "257/257":
                        t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightgreen)]))
                    elif entry == "256/257":
                        t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightyellow)]))
                    elif entry == "FAIL" or str(entry).endswith("/257"):
                        t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightpink)]))
                    if row_index > 0 and entry == row[-1]:
                        if entry == "ERROR":
                            t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightpink)]))
                        elif float(entry) > 7.976:
                            t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightgreen)]))
                        elif 7.9 <= float(entry) <= 7.976:
                            t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightyellow)]))
                        else:
                            t.setStyle(TableStyle([('BACKGROUND', (col_index, row_index), (col_index, row_index), colors.lightpink)]))
            final_table.append(t)
            doc.build(final_table)

    list_formatted_results = []
    for input_file, result_procedure_A, result_procedure_B in zip(input_files, results_procedure_A, results_procedure_B):
        formatted_results = format_results(input_file, result_procedure_A, result_procedure_B, details_level, PLL)
        list_formatted_results.append(formatted_results)
    write_results(output_filename, output_format, list_formatted_results, PLL)
    if output_format == 'both':
        log.info("Results written to both file {0}.csv and file {0}.pdf".format(output_filename))
    else:
        log.info("Results written to file {}.{}".format(output_filename, output_format))
