# coding: utf-8
import numpy as np
import argparse
import time
import logging as log
import os

from write_results_to_file import write_results_to_file
from procedure_A import procedure_A
from procedure_B import procedure_B

# shifts_5000 = np.add(np.arange(1, 5001), np.arange(0, 5000)[:,np.newaxis])


def AIS_31(input_file, reuse, cpu):
    """
    AIS-31 statistical tests
    """
    t0 = time.perf_counter()
    raw_input_data = np.unpackbits(np.fromfile(input_file, dtype='uint8'))
    log.info("Loaded {:.1E} random bits from {}".format(len(raw_input_data), input_file))
    results_procedure_A = procedure_A(raw_input_data[:8285728], cpu)
    if reuse:
        results_procedure_B = procedure_B(raw_input_data)
    else:
        results_procedure_B = procedure_B(raw_input_data[8285728:])
    log.info("AIS-31 tests passed in {}s".format(round(time.perf_counter()-t0, 2)))
    return results_procedure_A, results_procedure_B


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='AIS-31 tests')
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-l', '--log_level', default=log.INFO)
    parser.add_argument('-o', '--output_filename', type=str, nargs='?')
    parser.add_argument('-f', '--output_format', type=str, nargs='?', choices=["csv", "pdf", "both"])
    parser.add_argument('-d', '--details_level', type=int, choices=[1, 2], default=2)
    parser.add_argument('-r', '--reuse', action='store_true', help='Reuse same data in Procedure B as in Procedure A')
    parser.add_argument('--PLL', action='store_true', help='Enrich table if files are named like xxxxxx_m0_n0_m1_n1.npy')
    parser.add_argument('-c', '--cpu', type=int, default=1, help='Number of cores to use for parallel computation of T1 to T5')

    args = parser.parse_args()
    if type(args.log_level) != int:
        log.basicConfig(format='%(levelname)s:%(message)s', level=getattr(log, args.log_level.upper(), None))
    else:
        log.basicConfig(format='%(levelname)s:%(message)s', level=args.log_level)

    list_results_procedure_A = []
    list_results_procedure_B = []
    if args.output_filename:
        if os.path.isfile(args.output_filename+"."+args.output_format):
            raise IOError("Output file exists already, consider changing the name")
    for file_index, input_file in enumerate(sorted(args.input_files)):
        log.info("Processing file {}/{}".format(file_index+1, len(args.input_files)))
        results_procedure_A, results_procedure_B = AIS_31(input_file, args.reuse, args.cpu)
        list_results_procedure_A.append(results_procedure_A)
        list_results_procedure_B.append(results_procedure_B)
    if args.output_filename:
        write_results_to_file(args.input_files,
                              list_results_procedure_A,
                              list_results_procedure_B,
                              args.output_filename, args.output_format,
                              args.details_level, args.PLL)
    else:
        print("Results Procedure A")
        print(list_results_procedure_A)
        print("Results Procedure B")
        print(list_results_procedure_B)
