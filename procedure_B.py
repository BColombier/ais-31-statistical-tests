import numpy as np
from scipy.stats import chi2
import logging as log


def T6(raw_input_data):
    """
    AIS-31 T6 test
    Uniform distribution test
    """
    def get_value(raw_input_data):
        return np.sum(raw_input_data)/len(raw_input_data)

    def test_value(value):
        k = 1
        a = 0.025
        return 2**(-k)-a < value < 2**k+a

    log.info("Performing T6 test")
    value = get_value(raw_input_data)
    return value, test_value(value)


def T7(value, h, s):
    """
    AIS-31 T7 test
    Test for homogeneity
    """
    log.info("Performing T7 test")
    alpha = 0.0001
    return value < chi2.isf(alpha, (h-1)*(s-1))


def B_step_2(raw_input_data):
    """
    AIS-31, Procedure B, Step 2
    empirical 1-step transition probabilities
    """
    def get_value(raw_input_data):
        log.info("Performing Step 2")
        if len(raw_input_data)%2 == 0:
            two_bit_blocks = np.reshape(raw_input_data, (-1, 2))
        else:
            two_bit_blocks = np.reshape(raw_input_data[:-(len(raw_input_data)%2)], (-1, 2))
        starts_with_0 = []
        starts_with_1 = []
        for index, block in enumerate(two_bit_blocks):
            if len(starts_with_0) >= 100000 and len(starts_with_1) >= 100000:
                break
            if block[0] == 0:
                starts_with_0.append(block)
            else:
                starts_with_1.append(block)
        starts_with_0 = np.array(starts_with_0[:100000])
        starts_with_1 = np.array(starts_with_1[:100000])
        v_emp_01 = np.unique(starts_with_0, return_counts=True, axis=0)[1][1]
        v_emp_10 = np.unique(starts_with_1, return_counts=True, axis=0)[1][0]
        value = np.abs(v_emp_01/100000+v_emp_10/100000-1)
        return value, raw_input_data[2*index:]

    def test_value(value):
        return value < 0.02
    log.info("Performing Step 2")
    value, raw_input_data_left = get_value(raw_input_data)
    return raw_input_data_left, value, test_value(value)


def B_step_3(raw_input_data):
    """
    AIS-31, Procedure B, Step 3
    empirical 2-step transition probabilities
    """
    log.info("Performing Step 3")
    if len(raw_input_data)%3 == 0:
        three_bit_blocks = np.reshape(raw_input_data, (-1, 3))
    else:
        three_bit_blocks = np.reshape(raw_input_data[:-(len(raw_input_data)%3)], (-1, 3))
    v_emp_000, v_emp_001, v_emp_010, v_emp_011 = 0, 0, 0, 0
    v_emp_100, v_emp_101, v_emp_110, v_emp_111 = 0, 0, 0, 0
    for index, block in enumerate(three_bit_blocks):
        if block[0] == 0:
            if block[1] == 0:
                if (v_emp_000+v_emp_001) < 100000:
                    if block[2] == 0:
                        v_emp_000 += 1
                    else:
                        v_emp_001 += 1
            else:
                if (v_emp_010+v_emp_011) < 100000:
                    if block[2] == 0:
                        v_emp_010 += 1
                    else:
                        v_emp_011 += 1
        else:
            if block[1] == 0:
                if (v_emp_100+v_emp_101) < 100000:
                    if block[2] == 0:
                        v_emp_100 += 1
                    else:
                        v_emp_101 += 1
            else:
                if (v_emp_110+v_emp_111) < 100000:
                    if block[2] == 0:
                        v_emp_110 += 1
                    else:
                        v_emp_111 += 1
        if index > 400000:
            if (v_emp_000+v_emp_001) >= 100000 and\
               (v_emp_010+v_emp_011) >= 100000 and\
               (v_emp_100+v_emp_101) >= 100000 and\
               (v_emp_110+v_emp_111) >= 100000:
                break
    value_1 = (((v_emp_000-v_emp_100)**2)/(v_emp_000+v_emp_100)) + \
              (((v_emp_001-v_emp_101)**2)/(v_emp_001+v_emp_101))
    value_2 = (((v_emp_010-v_emp_110)**2)/(v_emp_010+v_emp_110)) + \
              (((v_emp_011-v_emp_111)**2)/(v_emp_011+v_emp_111))
    return raw_input_data[3*index:], \
        value_1, value_2, \
        T7(value_1, 2, 2), T7(value_2, 2, 2)


def B_step_4(raw_input_data):
    """
    AIS-31, Procedure B, Step 4
    empirical 3-step transition probabilities
    """
    log.info("Performing Step 4")
    if len(raw_input_data)%4 == 0:
        four_bit_blocks = np.reshape(raw_input_data, (-1, 4))
    else:
        four_bit_blocks = np.reshape(raw_input_data[:-(len(raw_input_data)%4)], (-1, 4))
    v_emp_0000, v_emp_0001, v_emp_0010, v_emp_0011 = 0, 0, 0, 0
    v_emp_0100, v_emp_0101, v_emp_0110, v_emp_0111 = 0, 0, 0, 0
    v_emp_1000, v_emp_1001, v_emp_1010, v_emp_1011 = 0, 0, 0, 0
    v_emp_1100, v_emp_1101, v_emp_1110, v_emp_1111 = 0, 0, 0, 0
    for index, block in enumerate(four_bit_blocks):
        if block[0] == 0:
            if block[1] == 0:
                if block[2] == 0:
                    if (v_emp_0000+v_emp_0001) < 100000:
                        if block[3] == 0:
                            v_emp_0000 += 1
                        else:
                            v_emp_0001 += 1
                else:
                    if (v_emp_0010+v_emp_0011) < 100000:
                        if block[3] == 0:
                            v_emp_0010 += 1
                        else:
                            v_emp_0011 += 1
            else:
                if block[2] == 0:
                    if (v_emp_0100+v_emp_0101) < 100000:
                        if block[3] == 0:
                            v_emp_0100 += 1
                        else:
                            v_emp_0101 += 1
                else:
                    if (v_emp_0110+v_emp_0111) < 100000:
                        if block[3] == 0:
                            v_emp_0110 += 1
                        else:
                            v_emp_0111 += 1
        else:
            if block[1] == 0:
                if block[2] == 0:
                    if (v_emp_1000+v_emp_1001) < 100000:
                        if block[3] == 0:
                            v_emp_1000 += 1
                        else:
                            v_emp_1001 += 1
                else:
                    if (v_emp_1010+v_emp_1011) < 100000:
                        if block[3] == 0:
                            v_emp_1010 += 1
                        else:
                            v_emp_1011 += 1
            else:
                if block[2] == 0:
                    if (v_emp_1100+v_emp_1101) < 100000:
                        if block[3] == 0:
                            v_emp_1100 += 1
                        else:
                            v_emp_1101 += 1
                else:
                    if (v_emp_1110+v_emp_1111) < 100000:
                        if block[3] == 0:
                            v_emp_1110 += 1
                        else:
                            v_emp_1111 += 1
        if index > 800000:
            if (v_emp_0000+v_emp_0001) >= 100000 and\
               (v_emp_0010+v_emp_0011) >= 100000 and\
               (v_emp_0100+v_emp_0101) >= 100000 and\
               (v_emp_0110+v_emp_0111) >= 100000 and\
               (v_emp_1000+v_emp_1001) >= 100000 and\
               (v_emp_1010+v_emp_1011) >= 100000 and\
               (v_emp_1100+v_emp_1101) >= 100000 and\
               (v_emp_1110+v_emp_1111) >= 100000:
                break
        if index > 10000000:
            return raw_input_data, -1, -1, -1, -1, False, False, False, False

    value_1 = (((v_emp_0000-v_emp_1000)**2)/(v_emp_0000+v_emp_1000)) + \
              (((v_emp_0001-v_emp_1001)**2)/(v_emp_0001+v_emp_1001))
    value_2 = (((v_emp_0100-v_emp_1100)**2)/(v_emp_0100+v_emp_1100)) + \
              (((v_emp_0101-v_emp_1101)**2)/(v_emp_0101+v_emp_1101))
    value_3 = (((v_emp_0010-v_emp_1010)**2)/(v_emp_0010+v_emp_1010)) + \
              (((v_emp_0011-v_emp_1011)**2)/(v_emp_0011+v_emp_1011))
    value_4 = (((v_emp_0110-v_emp_1110)**2)/(v_emp_0110+v_emp_1110)) + \
              (((v_emp_0111-v_emp_1111)**2)/(v_emp_0111+v_emp_1111))
    return raw_input_data[4*index:], value_1, value_2, value_3, value_4, \
        T7(value_1, 3, 3), T7(value_3, 3, 2), T7(value_3, 3, 3), T7(value_4, 3, 3)


def T8(raw_input_data):
    """
    AIS-31 T8 test
    Shannon entropy/byte estimation
    """
    def get_value(raw_input_data):
        L = 8
        Q = 2560
        K = 256000
        if len(raw_input_data) < (Q+K)*L:
            raise ValueError("Not enough data to perform entropy estimation")
        byte_sequence = np.packbits(raw_input_data[:(Q+K)*L])
        # byte_sequence = (Q+K)*np.arange(256)
        last_pos = np.zeros(256, dtype='uint32')
        g_vals = np.ones(256000, dtype='float64')
        T = 0.0
        for index, byte in enumerate(byte_sequence):
            if last_pos[byte] != 0:
                gap = index-last_pos[byte]
                if gap > len(g_vals):
                    raise ValueError("No precomputed g_val for this gap, you should increase the size of the g_vals array (currently {})".format(len(g_vals)))
                if index >= Q:
                    if g_vals[gap] == 1:
                        g_vals[gap] = (1/np.log(2))*np.sum([1/i for i in range(1, gap)])
                    T += g_vals[gap]
            last_pos[byte] = index
        if index+1 != (Q+K):
            raise ValueError("Not enough data to perform entropy estimation")
        return T/K

    def test_value(value):
        return value > 7.976

    log.info("Performing T8 test")
    try:
        value = get_value(raw_input_data)
    except ValueError as e:
        log.warning(e)
        return 0, "ERROR"
    return value, test_value(value)


def procedure_B(raw_input_data):
    """
    AIS-31 Procedure B
    Runs tests T6 to T8
    """
    log.info("Starting Procedure B")
    T6_result = T6(raw_input_data[:100000])
    raw_input_data_left, *B_step_2_result = B_step_2(raw_input_data[100000:])
    try:
        raw_input_data_left, *B_step_3_result = B_step_3(raw_input_data_left)
    except ZeroDivisionError:
        print("ZeroDivisionError in Procedure B, Step 3")
        B_step_3_result = [0, 0, False, False]
    try:
        raw_input_data_left, *B_step_4_result = B_step_4(raw_input_data_left)
    except ZeroDivisionError:
        print("ZeroDivisionError in Procedure B, Step 4")
        B_step_4_result = [0, 0, False, False]
    T8_result = T8(raw_input_data_left)
    log.info("Procedure B finished")
    return T6_result, B_step_2_result, B_step_3_result, B_step_4_result, T8_result
