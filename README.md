# AIS-31 tests in Python

Python version of the AIS-31 tests (as defined in KS2011.pdf).

## Usage

Basic usage:

```bash
python3 AIS_31.py -l LOG_LEVEL -o OUTPUT_FILENAME -f FORMAT -d DETAILS_LEVEL -c 4
```

Reuse the same data in Procedure A and Procedure B:

```bash
python3 AIS_31.py -l LOG_LEVEL -o OUTPUT_FILENAME -f FORMAT -d DETAILS_LEVEL -c N_CPUs --reuse
```

### Options

#### LOG_LEVEL

Level of log output in the terminal. Can be either:  
- `debug`  : a lot of information,  
- `info`   : just the necessary,  
- `warning`: nothing.

'info' is the default.

#### OUTPUT_FILENAME

Name of the results file, **without** extension.

#### FORMAT

Can be either `csv`, `pdf` or `both`. PDF is nicer.

#### DETAILS_LEVEL

Can be either:
- `1`: only `PASS` or `FAIL`,
- `2`: `PASS` or `FAIL` for T0 and T6, `XXX/257` for T1 to T5, entropy estimation for T8.

`2` is the default.

#### N_CPUs

Number of CPUs to use for the parallel computation of tests T1 to T5.

1 is the default.